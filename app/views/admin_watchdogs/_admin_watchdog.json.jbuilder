json.extract! admin_watchdog, :id, :admin_id, :logintime, :ipaddress, :lastvisit, :created_at, :updated_at
json.url admin_watchdog_url(admin_watchdog, format: :json)
