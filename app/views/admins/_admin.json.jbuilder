json.extract! admin, :id, :uuid, :roleid, :username, :firstname, :lastname, :language, :disabled, :loginattempts, :email, :encrypted_password, :confirmation_token, :remember_token, :created_at, :updated_at
json.url admin_url(admin, format: :json)
