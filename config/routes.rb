
Rails.application.routes.draw do

  resources :admins
  resources :admin_watchdogs
  resources :users
  
  resources :passwords,
    controller: 'clearance/passwords',
    only: [:create, :new]

  resource :session,
    controller: 'clearance/sessions',
    only: [:create]

  resources :admins,
    controller: 'clearance/admins',
    only: Clearance.configuration.user_actions do
      resource :password,
        controller: 'clearance/passwords',
        only: [:create, :edit, :update]
    end

  get '/sign_in' => 'clearance/sessions#new', as: 'sign_in'
  delete '/sign_out' => 'clearance/sessions#destroy', as: 'sign_out'

  if Clearance.configuration.allow_sign_up?
    get '/sign_up' => 'clearance/admins#new', as: 'sign_up'
  end

  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root "admins#index"
end