class AddClearanceToAdmins < ActiveRecord::Migration[5.2]
  def self.up
    change_table :admins do |t|
      t.string :email
      t.string :encrypted_password, limit: 128
      t.string :confirmation_token, limit: 128
      t.string :remember_token, limit: 128
    end

    add_index :admins, :email
    add_index :admins, :remember_token

    admins = select_all("SELECT id FROM admins WHERE remember_token IS NULL")

    admins.each do |admin|
      update <<-SQL
        UPDATE admins
        SET remember_token = '#{Clearance::Token.new}'
        WHERE id = '#{admin['id']}'
      SQL
    end
  end

  def self.down
    change_table :admins do |t|
      t.remove :email, :encrypted_password, :confirmation_token, :remember_token
    end
  end
end
