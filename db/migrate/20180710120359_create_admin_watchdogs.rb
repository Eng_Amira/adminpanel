class CreateAdminWatchdogs < ActiveRecord::Migration[5.2]
  def change
    create_table :admin_watchdogs do |t|
      t.text :admin_id
      t.datetime :logintime
      t.text :ipaddress
      t.datetime :lastvisit

      t.timestamps
    end
  end
end
