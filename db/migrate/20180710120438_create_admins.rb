class CreateAdmins < ActiveRecord::Migration[5.2]
  def change
    create_table :admins do |t|
      t.integer :uuid
      t.integer :roleid
      t.string :username
      t.string :firstname
      t.string :lastname
      t.string :language
      t.integer :disabled
      t.integer :loginattempts
      

      t.timestamps
    end
  end
end
