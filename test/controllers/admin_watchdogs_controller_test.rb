require 'test_helper'

class AdminWatchdogsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_watchdog = admin_watchdogs(:one)
  end

  test "should get index" do
    get admin_watchdogs_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_watchdog_url
    assert_response :success
  end

  test "should create admin_watchdog" do
    assert_difference('AdminWatchdog.count') do
      post admin_watchdogs_url, params: { admin_watchdog: { admin_id: @admin_watchdog.admin_id, ipaddress: @admin_watchdog.ipaddress, lastvisit: @admin_watchdog.lastvisit, logintime: @admin_watchdog.logintime } }
    end

    assert_redirected_to admin_watchdog_url(AdminWatchdog.last)
  end

  test "should show admin_watchdog" do
    get admin_watchdog_url(@admin_watchdog)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_watchdog_url(@admin_watchdog)
    assert_response :success
  end

  test "should update admin_watchdog" do
    patch admin_watchdog_url(@admin_watchdog), params: { admin_watchdog: { admin_id: @admin_watchdog.admin_id, ipaddress: @admin_watchdog.ipaddress, lastvisit: @admin_watchdog.lastvisit, logintime: @admin_watchdog.logintime } }
    assert_redirected_to admin_watchdog_url(@admin_watchdog)
  end

  test "should destroy admin_watchdog" do
    assert_difference('AdminWatchdog.count', -1) do
      delete admin_watchdog_url(@admin_watchdog)
    end

    assert_redirected_to admin_watchdogs_url
  end
end
