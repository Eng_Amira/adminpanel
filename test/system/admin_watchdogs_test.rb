require "application_system_test_case"

class AdminWatchdogsTest < ApplicationSystemTestCase
  setup do
    @admin_watchdog = admin_watchdogs(:one)
  end

  test "visiting the index" do
    visit admin_watchdogs_url
    assert_selector "h1", text: "Admin Watchdogs"
  end

  test "creating a Admin watchdog" do
    visit admin_watchdogs_url
    click_on "New Admin Watchdog"

    fill_in "Admin", with: @admin_watchdog.admin_id
    fill_in "Ipaddress", with: @admin_watchdog.ipaddress
    fill_in "Lastvisit", with: @admin_watchdog.lastvisit
    fill_in "Logintime", with: @admin_watchdog.logintime
    click_on "Create Admin watchdog"

    assert_text "Admin watchdog was successfully created"
    click_on "Back"
  end

  test "updating a Admin watchdog" do
    visit admin_watchdogs_url
    click_on "Edit", match: :first

    fill_in "Admin", with: @admin_watchdog.admin_id
    fill_in "Ipaddress", with: @admin_watchdog.ipaddress
    fill_in "Lastvisit", with: @admin_watchdog.lastvisit
    fill_in "Logintime", with: @admin_watchdog.logintime
    click_on "Update Admin watchdog"

    assert_text "Admin watchdog was successfully updated"
    click_on "Back"
  end

  test "destroying a Admin watchdog" do
    visit admin_watchdogs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Admin watchdog was successfully destroyed"
  end
end
